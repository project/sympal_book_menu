## Readme for sympal_book_menu


## About
This module adds your book_hierarchy automagically into your navigation. Very straightforward, and extremely simple.
It provides no configuration, no database and so on. Just enable the module and refresh your menu's (cache).

The simplicity is its good part: You don't need to explain your users why the core book block disappears on several places, nor do you need to explain how to add menu items on each node.
That latter feature is in core, and it allows much more power. But it also disconnects things, such as the book hierarchy and menu hierarchy. It disconnects the node titles from the menu titles. While for many these are considered 'features', many people prefer simplicity. Another good reason why you  may want to use this module, is that this one does not require you to hand out menu administration permissions: anyone who can publish a book in the hierarchy will add it to the menu!

## Credits
By Bèr Kessels, webschuur.com ber@webschuur.com
For www.Sympal.nl
First released for Drupal 4.7
[Homepage](http://drupal.org/projects)
                                     _         _
     ___ _   _ _ __ ___  _ __   __ _| |  _ __ | |
    / __| | | | '_ ` _ \| '_ \ / _` | | | '_ \| |
    \__ \ |_| | | | | | | |_) | (_| | |_| | | | |
    |___/\__, |_| |_| |_| .__/ \__,_|_(_)_| |_|_|
         |___/          |_|

 -readme written in markdown-